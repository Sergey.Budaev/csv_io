# CSV_IO STANDALONE #

This is a stand-alone version of the CSV_IO module from HEDTOOLS that
can be used independently of all other HEDTOOLS procedures.

See [https://ahamodel.uib.no/doc/#_module_csv_io](CSV_IO Manual)

## Tests

To build the test program in `tests` directory do this:

    gfortran -o csvtest m_CSV_IO.f90 tests/tests.f90

and then run the `csvtest` program.

