! This program illustrates how to convert an array of derived type, some
! data component of which is also an array into a two-dimensional array.
! This may be helpful to use the CSV_MATRIX_WRITE subroutine to save data
! array into a CSV file.
! Note: The code uses the DISPMODULE
!       Jonasson, K. 2009. Algorithm 892: DISPMODULE, a Fortran 95 module for
!       pretty-printing matrices. ACM Trans. Math. Softw. 36, 1, Article 6
!       (March 2009), 7 pages. DOI: https://doi.org/10.1145/1486525.1486531.
!       Code: https://github.com/radiganm/dispmodule
!-------------------------------------------------------------------------------
! Compile and build this demo:
!   gfortran m_dispmd.f90 ../m_CSV_IO.f90 demo_dt.f90
!-------------------------------------------------------------------------------
program test_csv_type

  use CSV_IO, only : CSV_MATRIX_WRITE
  use DISPMODULE, only: DISP ! DISPMODULE is used for pretty printing

  integer, parameter :: TYPE_MAX = 3, N_OBJECTS = 5
  integer :: i,j
  real, parameter :: MISSING = -9999.0

  ! Declare the derived type `struct_def`
  type :: struct_def
    real    :: real_comp
    integer :: intg_comp
    real, dimension(TYPE_MAX) :: array_comp
  end type struct_def

  ! Declare an array of the objects of the type `struct_def`
  type(struct_def), dimension(N_OBJECTS) :: objects_array

  ! Temporary array
  real, dimension(N_OBJECTS, TYPE_MAX) :: tmp_data

  ! Initialise the objects array of the type `struct_def`
  ! using the type constructor.
  objects_array(1) = struct_def( 1.0, 1, [1.1, 1.2, 1.3]  )
  objects_array(2) = struct_def( 2.0, 2, [2.1, 2.2, 2.3]  )
  objects_array(3) = struct_def( 3.0, 3, [3.1, 3.2, 3.3]  )
  objects_array(4) = struct_def( 4.0, 4, [4.1, 4.2, 4.3]  )
  objects_array(5) = struct_def( 5.0, 5, [5.1, 5.2, 5.3]  )

  !-----------------------------------------------------------------------------

  ! Convert the derived type components into a 2-D temporary array
  do i = 1, N_OBJECTS
   do j=1, TYPE_MAX
     tmp_data(i,j) = objects_array(i)%array_comp(j)
   end do
  end do

  print *, "Array composed from the derived type through nested loops:"
  call disp("  tmp array = ", tmp_data)

  ! Write the temporary array to the CSV output file.
  call CSV_MATRIX_WRITE( tmp_data, "data_file_1_tmp_loops.csv" )

  print *, ""
  !-----------------------------------------------------------------------------
  print *, ""

  print *, "Array composed from the derived type inline, via implied loops:"
  call disp( "  construct = ",                                                &
      reshape( [( [( objects_array(i)%array_comp(j), i=1,N_OBJECTS )],        &
                                                          j=1, TYPE_MAX  )],  &
               [N_OBJECTS,TYPE_MAX] ) )

  ! The derived type can be converted to the array inline by combination of
  ! reshape with two implied loops. This is a short version of the code.
  call CSV_MATRIX_WRITE(                                                      &
      reshape( [( [( objects_array(i)%array_comp(j), i=1,N_OBJECTS )],        &
                                                          j=1, TYPE_MAX  )],  &
               [N_OBJECTS,TYPE_MAX] ),                                        &
      "data_file_2_inline.csv"  )

end program test_csv_type


