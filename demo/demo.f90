! This program illustrates how to copy data across two arrays, one
! of which is allocatable (X) and is read from CSV file, whereas the other
! is fixed non-allocatable (Y).
! Then, the data read from the CSV file (X), and only these data are copied
! into the fixed array Y.
! Note: The code uses the DISPMODULE
!       Jonasson, K. 2009. Algorithm 892: DISPMODULE, a Fortran 95 module for
!       pretty-printing matrices. ACM Trans. Math. Softw. 36, 1, Article 6
!       (March 2009), 7 pages. DOI: https://doi.org/10.1145/1486525.1486531.
!       Code: https://github.com/radiganm/dispmodule
!-------------------------------------------------------------------------------
! Compile and build this demo:
!   gfortran m_dispmd.f90 ../m_CSV_IO.f90 demo.f90
!-------------------------------------------------------------------------------
program test_part_matrix

  use CSV_IO, only : CSV_MATRIX_READ, CSV_MATRIX_WRITE
  use DISPMODULE, only: DISP ! DISPMODULE is used for pretty printing

  ! 'X' is dynamic size array, it is allocated depending on the valid data that
  ! is read from the CSV data file. So, 'X' can be either small or big as the
  ! data in the file.
  real, allocatable, dimension(:,:) :: X

  ! 'Y' data is fixed size array, change dimensions to play with the data.
  real, dimension(20,8) :: Y

  integer :: xrows, xcols,  yrows, ycols

  X = CSV_MATRIX_READ("zzz.csv", missing_code = -9999.0)

  xrows = size(X,1)
  xcols = size(X,2)

  print *, "X (CSV) size ", xrows, xcols
  print *, "X (CSV) is allocated automatically ", allocated(X)
  call DISP(' X = ',  X)
  print *, "------------------------------------"

  ! Initialize the 'Y' matrix with some empty or missing code, or zero data.
  Y = -9999.0

  yrows = size(Y, 1)
  ycols = size(Y, 2)
  print *, "Y size ", yrows, ycols, "(fixed array)"

  ! Here we check if 'Y' matrix dimensions are bigger than 'X' (that comes
  ! from  the CSV file and is allocatable/dynamic). If yes, only a subset of
  ! the 'Y' is filled with data from 'X'. If 'Y' is smaller than the 'X' data,
  ! then only the fitting part of Y is filled by data from X.
  if ( yrows > xrows ) yrows = xrows
  if ( ycols > xcols ) ycols = xcols

  Y( 1:yrows, 1:ycols ) = X     ! Copy only the X-sized part of data into Y

  call DISP(' Y = ', Y)

  call CSV_MATRIX_WRITE(Y,"zzz_out.csv")

end program test_part_matrix
